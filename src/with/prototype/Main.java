package with.prototype;

public class Main {
	// Number of beers in each array
	private static final int NUMBER = 6;

	public static void main(String[] args) {
		// Declare one array for one six-pack
		Beer[] norBeers = new Beer[NUMBER];

		// Create new Beer object on the first index
		norBeers[0] = new Beer();

		// Set the attributes once
		norBeers[0].setCountry("Norway");
		norBeers[0].setAmount(0.5f);
		norBeers[0].setPercent(4.7f);
		norBeers[0].setPrice(32.0f);

		// Loop through all, except the first since they all clone the first object
		for (int i = 1; i < NUMBER; i++) {
			// Not like this
			// norBeers[i] = norBeers[0];

			// but like this: clone the one object 5(NUMBER - 1) more times
			norBeers[i] = (Beer) norBeers[0].makeCopy();
		}

		System.out.println("==With Prototype pattern==");
		// Loop through the beers and show hashCode
		for (int i = 0; i < NUMBER; i++) {
			System.out.println(norBeers[i] + " [" + System.identityHashCode(norBeers[i]) + "]");
		}

	}
}
