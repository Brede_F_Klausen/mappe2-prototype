package with.prototype;

public interface Product extends Cloneable {

	public Product makeCopy();
}