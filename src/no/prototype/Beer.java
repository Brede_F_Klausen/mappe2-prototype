package no.prototype;

public class Beer implements Product {
	private String country;
	private float price;
	private float amount;
	private float percent;

	public void setCountry(String country) {
		this.country = "Made in " + country;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public void setPercent(float percent) {
		this.percent = percent;
	}

	@Override
	public String toString() {
		if (!country.isEmpty())
			return country + " kr " + price + ", " + amount + "kg, " + percent + "%";
		else
			return "N/A";
	}

}
