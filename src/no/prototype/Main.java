package no.prototype;

public class Main {
	// Number of beers in each array
	private static final int NUMBER = 6;

	public static void main(String[] args) {
		// Declare one array for one six-pack
		Beer[] norBeers = new Beer[NUMBER];

		// Loop through the array
		for (int i = 0; i < NUMBER; i++) {
			// Create new Beer object
			norBeers[i] = new Beer();

			// Set the attributes in each loop
			norBeers[i].setCountry("Norway");
			norBeers[i].setAmount(0.5f);
			norBeers[i].setPercent(4.7f);
			norBeers[i].setPrice(32.0f);
		}

		System.out.println("==Without Prototype pattern==");
		// Loop through the beers and show hashCode
		for (int i = 0; i < NUMBER; i++) {
			System.out.println(norBeers[i] + " [" + System.identityHashCode(norBeers[i]) + "]");
		}
	}

}
